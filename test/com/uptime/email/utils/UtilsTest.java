/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.utils;

import com.uptime.email.EmailService;
import com.uptime.email.vo.EmailVO;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

/**
 *
 * @author aswani
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UtilsTest {

    private static EmailVO emailVO = null;

    public UtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        EmailService.MAIL_SERVER = "10.1.10.6";
        List<String> list = new ArrayList<>();
        emailVO = new EmailVO();
        list.add("aswani.kumar@maestrodigitalservices.com");
        emailVO.setToEmails(list);
        list = new ArrayList<>();
        list.add("joseph.k@maestrodigitalservices.com");
        emailVO.setCcEmails(list);
        emailVO.setSubject("Test Junit test cases");
        emailVO.setMailBody("Hi, Please ignore this mail");
        emailVO.setFromEmail("DoNotReply@uptime-solutions.us");
    }

    /**
     * Test of sendMail method, of class Utils.
     */
    @Test
    public void test1_SendMail() {
        Utils utils = new Utils();
        String expetedResult = "Mail Sent successfully";
        String result = utils.sendMail(emailVO);
        assertEquals(expetedResult, result);
    }
    /**
     * Test of sendMail method, of class Utils.
     */
    @Test
    public void test2_SendMail() {
        Utils utils = new Utils();
        String expetedResult = "Error while sending mail";
        List<String> list = new ArrayList<>();
        list.add("aswani.kumar");
        emailVO.setToEmails(list);
        String result = utils.sendMail(emailVO);
        assertEquals(expetedResult, result);
    }
    /**
     * Test of sendMail method, of class Utils.
     */
    @Test
    public void test3_SendMail() {
        Utils utils = new Utils();
        String expetedResult = "Error while sending mail";
        List<String> list = new ArrayList<>();
        list.add("aswani.kumar");
        emailVO.setCcEmails(list);
        String result = utils.sendMail(emailVO);
        assertEquals(expetedResult, result);
    }

}
