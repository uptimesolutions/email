/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.utils;

import static com.uptime.email.EmailService.LOGGER;
import static com.uptime.email.EmailService.MAIL_SERVER;
import static com.uptime.email.EmailService.sendEvent;
import com.uptime.email.vo.EmailVO;
import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author ksimmons
 */
public class Utils {
    /**
     * Sends an email with the given info
     * @param email, EmailVO
     * @return 
     */
    public String sendMail(EmailVO email) {
        Properties props;
        MimeBodyPart bodyPart = new MimeBodyPart();
        MimeBodyPart attachment;
        Multipart multiPart = new MimeMultipart();
        MimeMessage message;
        Session session;
        InternetAddress address;
        InternetAddress[] addresses;
        int count;
        
        try{
            props = new Properties();
            props.put("mail.smtp.host", MAIL_SERVER);
            session = Session.getDefaultInstance(props);
            message = new MimeMessage(session);
            
            // Test and Set From Emails
            address = new InternetAddress(email.getFromEmail());
            address.validate();
            message.setFrom(address);

            // Test and Set To Emails
            addresses = new InternetAddress[email.getToEmails().size()];
            count = email.getToEmails().size();
            for (int i = 0; i < email.getToEmails().size(); i++) {
                addresses[i] = new InternetAddress(email.getToEmails().get(i).trim());
                addresses[i].validate();
            }
            message.setRecipients(Message.RecipientType.TO, addresses);  

            // Test and Set CC Emails
            if (email.getCcEmails() != null && !email.getCcEmails().isEmpty()) {
                addresses = new InternetAddress[email.getCcEmails().size()];
                for (int i = 0; i < email.getCcEmails().size(); i++) {
                    addresses[i] = new InternetAddress(email.getCcEmails().get(i).trim());
                    addresses[i].validate();
                }
                message.setRecipients(Message.RecipientType.CC, addresses);            
            }  

            // Set Body for EmailService
            bodyPart.setContent(email.getMailBody(), "text/html; charset=utf-8");   
            multiPart.addBodyPart(bodyPart);   

            // Set Attachments for EmailService
            if (email.getAttachments() != null && !email.getAttachments().isEmpty()) {
                if(email.getAttachmentContents() != null && !email.getAttachmentContents().isEmpty()) {
                    for(int i = 0; i < email.getAttachments().size(); i++) {
                        attachment = new MimeBodyPart();
                        attachment.setFileName(email.getAttachments().get(i));
                        attachment.attachFile(email.getAttachmentContents().get(i));
                        multiPart.addBodyPart(attachment);
                    }
                } else {
                    for(String file : email.getAttachments()){
                        attachment = new MimeBodyPart();
                        attachment.setFileName(Paths.get(file).getFileName().toString());
                        attachment.attachFile(new File(file));
                        multiPart.addBodyPart(attachment);
                    }
                }
            }
            
            message.setContent(multiPart);
            message.setSentDate(new Date()); 
            message.setSubject(email.getSubject());
            Transport.send(message);
            LOGGER.log(Level.INFO, "Attempting to send mail to relay: "+MAIL_SERVER);
            
            if(count > 1) {
                //System.out.println("Email sent to " + count + " recipients.");
                LOGGER.log(Level.INFO, "Email sent to {0} recipients.", count);
            } else {
                //System.out.println("Email sent to a recipient.");
                LOGGER.log(Level.INFO, "Email sent to {0} recipients.", count);
            }
            
        } catch(MessagingException e) {        
            //System.out.println("Failed to send email.");
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return "Error while sending mail";
        }  
        catch(Exception e) {
            sendEvent(e.getStackTrace());
            //System.out.println("Failed to send email.");
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return "Error while sending mail";
        }
        return "Mail Sent successfully";
    }
}
