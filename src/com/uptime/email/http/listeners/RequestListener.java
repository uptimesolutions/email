/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.http.listeners;

import com.sun.net.httpserver.HttpServer;
import static com.uptime.email.EmailService.LOGGER;
import com.uptime.email.http.handlers.EmailHandler;
import com.uptime.email.http.handlers.SystemHandler;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.logging.Level;

/**
 *
 * @author ksimmons
 */
public class RequestListener {
    private int port = 0;
    HttpServer server;
    
    public RequestListener(int port) {
        this.port = port;
    }
    
    public void stop() {
        LOGGER.log(Level.INFO, "Stopping request listener...");
        server.stop(10);
    }
    
    public void start() throws Exception {
        server = HttpServer.create(new InetSocketAddress(port), 2);
        server.createContext("/system", new SystemHandler());
        server.createContext("/email", new EmailHandler());
        server.setExecutor(Executors.newCachedThreadPool());
        
        //start server
        LOGGER.log(Level.INFO, "Starting request listener...");
        server.start();
    }
}
