/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.http.handlers;

import com.uptime.services.http.handler.AbstractSystemHandler;
import static com.uptime.email.EmailService.IP_ADDRESS;
import static com.uptime.email.EmailService.LOGGER;
import static com.uptime.email.EmailService.PORT;
import static com.uptime.email.EmailService.SERVICE_NAME;
import static com.uptime.email.EmailService.mutex;
import static com.uptime.email.EmailService.names;
import static com.uptime.email.EmailService.running;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class SystemHandler extends AbstractSystemHandler {
    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public void setRunning(boolean value) {
        running = value;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }

    @Override
    public String getServiceName() {
        return SERVICE_NAME;
    }

    @Override
    public int getServicePort() {
        return PORT;
    }

    @Override
    public String getServiceIpAddress() {
        return IP_ADDRESS;
    }

    @Override
    public String[] getServiceSubscribeNames() {
        return names;
    }

    @Override
    public Semaphore getMutex() {
        return mutex;
    }

    @Override
    public void sendEvent(StackTraceElement[] stackTrace) {
        com.uptime.email.EmailService.sendEvent(stackTrace);
    }
}
