/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.http.handlers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import static com.uptime.email.EmailService.LOGGER;
import static com.uptime.email.EmailService.running;
import static com.uptime.email.EmailService.sendEvent;
import com.uptime.email.http.handlers.multipart.FileItem;
import com.uptime.email.http.handlers.multipart.Multipart;
import com.uptime.email.http.handlers.multipart.PartHandler;
import com.uptime.email.utils.Utils;
import com.uptime.email.vo.EmailVO;
import com.uptime.services.http.handler.AbstractGenericHandler;
import static com.uptime.services.util.HttpUtils.parseQuery;
import static com.uptime.services.util.ServiceUtil.streamReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ksimmons
 */
public class EmailHandler extends AbstractGenericHandler {
    final private Utils util;
    
    public EmailHandler() {
        util = new Utils();
    }
    
    /**
     * HTTP GET handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doGet(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        Map<String, Object> params;
        byte [] response;
        OutputStream os;
        Headers respHeaders = he.getResponseHeaders();
        respHeaders.set("Content-Type", "application/json");
                
        try {
            params = parseQuery(he.getRequestURI().getQuery());
            if(params.isEmpty()){
                response = "{\"outcome\":\"Hello World!\"}".getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length);
            }else{
                response = "{\"outcome\":\"Unknown Params\"}".getBytes();
                he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_ACCEPTABLE, response.length);
            }
        } catch (UnsupportedEncodingException e) {
            stackTrace = e.getStackTrace();
            response = "{\"outcome\":\"UnsupportedEncodingException\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            response = "{\"outcome\":\"Exception\"}".getBytes();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, response.length);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        
        os = he.getResponseBody();
        os.write(response);
        os.flush();
        he.close();
        
        // Send Event
        if(stackTrace != null) 
            sendEvent(stackTrace);
    }
    
    /**
     * HTTP POST handler
     * @param he
     * @throws IOException 
     */
    @Override
    public void doPost(HttpExchange he) throws IOException {
        StackTraceElement[] stackTrace = null;
        String content;
        JsonElement element;
        JsonObject json, subJson;
        JsonArray jsonArray;
        String value, tempFilePath;
        Multipart multipart;
        Map<String,String> multipartParams;
        List<String> values;
        EmailVO evo = null;
        boolean good = false;
        List<FileItem> files;
        File file;

        try {
            if(he.getRequestBody() != null) {
                if (he.getRequestHeaders().getFirst("MISUpload") != null && he.getRequestHeaders().getFirst("MISUpload").equals("true")) {
                    LOGGER.log(Level.INFO, "FILE SAVE IN: {0} ", (tempFilePath = System.getProperty("user.dir")));
                    multipartParams = new HashMap();
                    multipart = new Multipart();
                    files = new ArrayList();
                    evo = new EmailVO();

                    // parse the request
                    multipart.parse(he, new PartHandler(){
                        @Override
                        public void handleFormItem(String name, String value) {
                            multipartParams.put(name, value );
                        }

                        @Override
                        public  void handleFileItem(String name, FileItem fileItem) {
                            files.add(fileItem);
                        }
                    }, tempFilePath);

                    // get attachments from the request
                    for(FileItem item : files) {
                        if(evo.getAttachments() == null || evo.getAttachmentSizes() == null || evo.getAttachmentContents() == null) {
                            evo.setAttachments(new ArrayList());
                            evo.setAttachmentSizes(new ArrayList());
                            evo.setAttachmentContents(new ArrayList());
                        }

                        evo.getAttachments().add(item.getFileName().toUpperCase());
                        evo.getAttachmentContents().add(item.getFile());
                        evo.getAttachmentSizes().add(new Long(item.getContentLength()).intValue());
                    }
                    
                    // get the JSON from the request
                    if(evo.getAttachments() != null && !evo.getAttachments().isEmpty() && evo.getAttachmentContents() != null && !evo.getAttachmentContents().isEmpty() && evo.getAttachments().size() == evo.getAttachmentContents().size() && multipartParams.containsKey("emailJson")) {
                        if((content = multipartParams.get("emailJson")) != null && !content.isEmpty()) {
                            LOGGER.log(Level.INFO, "*********JSON VALUE:{0}", content);
                            if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("from_email") && json.has("to_emails") && json.has("subject") && json.has("body") && (element = json.get("from_email")) != null && (value = element.getAsString()) != null && !value.isEmpty()) {
                                //evo = new EmailVO();
                                evo.setFromEmail(value); // Setting fromEmail
                                if ((element = json.get("subject")) != null && (value = element.getAsString()) != null && !value.isEmpty()){
                                    evo.setSubject(value); // Setting email Subject
                                    if ((element = json.get("body")) != null && (value = element.getAsString()) != null && !value.isEmpty()){
                                        evo.setMailBody(value); // Setting email Body
                                        if ((jsonArray = json.getAsJsonArray("to_emails")) != null) {
                                            values = new ArrayList();
                                            for (JsonElement ele : jsonArray) {
                                                if(ele != null && (subJson = ele.getAsJsonObject()) != null && subJson.has("to_email") && (element = subJson.get("to_email")) != null && (value = element.getAsString()) != null && !value.isEmpty())
                                                    values.add(value);
                                            }
                                            if (values.size() > 0) {
                                                evo.setToEmails(values); // Setting toEmails

                                                // Setting ccEmails
                                                values = new ArrayList();
                                                if (json.has("cc_emails") && (jsonArray = json.getAsJsonArray("cc_emails")) != null) {
                                                    for (JsonElement ele : jsonArray) {
                                                        if(ele != null && (subJson = ele.getAsJsonObject()) != null && subJson.has("cc_email") && (element = subJson.get("cc_email")) != null && (value = element.getAsString()) != null && !value.isEmpty())
                                                            values.add(value);
                                                    }  
                                                } 
                                                evo.setCcEmails(values);

                                                good = true;
                                            }
                                        }
                                    }
                                }
                            } 
                        }
                    }
                } else {
                    if((content = streamReader(he.getRequestBody())) != null && !content.isEmpty()) {
                        LOGGER.log(Level.INFO, "*********JSON VALUE:{0}", content);
                        if ((element = JsonParser.parseString(content)) != null && (json = element.getAsJsonObject()) != null && json.has("from_email") && json.has("to_emails") && json.has("subject") && json.has("body") && (element = json.get("from_email")) != null && (value = element.getAsString()) != null && !value.isEmpty()) {
                            evo = new EmailVO();
                            evo.setFromEmail(value); // Setting fromEmail
                            if ((element = json.get("subject")) != null && (value = element.getAsString()) != null && !value.isEmpty()){
                                evo.setSubject(value); // Setting email Subject
                                if ((element = json.get("body")) != null && (value = element.getAsString()) != null && !value.isEmpty()){
                                    evo.setMailBody(value); // Setting email Body
                                    if ((jsonArray = json.getAsJsonArray("to_emails")) != null) {
                                        values = new ArrayList();
                                        for (JsonElement ele : jsonArray) {
                                            if(ele != null && (subJson = ele.getAsJsonObject()) != null && subJson.has("to_email") && (element = subJson.get("to_email")) != null && (value = element.getAsString()) != null && !value.isEmpty())
                                                values.add(value);
                                        }
                                        if (values.size() > 0) {
                                            evo.setToEmails(values); // Setting toEmails

                                            // Setting ccEmails
                                            values = new ArrayList();
                                            if (json.has("cc_emails") && (jsonArray = json.getAsJsonArray("cc_emails")) != null) {
                                                for (JsonElement ele : jsonArray) {
                                                    if(ele != null && (subJson = ele.getAsJsonObject()) != null && subJson.has("cc_email") && (element = subJson.get("cc_email")) != null && (value = element.getAsString()) != null && !value.isEmpty())
                                                        values.add(value);
                                                }  
                                            } 
                                            evo.setCcEmails(values);

                                            // Setting attachments
                                            values = new ArrayList();
                                            if (json.has("attachments") && (jsonArray = json.getAsJsonArray("attachments")) != null) {
                                                for (JsonElement ele : jsonArray) {
                                                    if(ele != null && (subJson = ele.getAsJsonObject()) != null && subJson.has("attachment") && (element = subJson.get("attachment")) != null && (value = element.getAsString()) != null && !value.isEmpty()) {
                                                        if ((file = new File(value)).exists() && file.canRead())
                                                            values.add(value);
                                                    }
                                                }
                                            }
                                            evo.setAttachments(values);

                                            good = true;
                                        }
                                    }
                                }
                            }
                        } 
                    } 
                }
            }
            he.sendResponseHeaders(good ? HttpURLConnection.HTTP_OK : HttpURLConnection.HTTP_NOT_ACCEPTABLE, 0);
        } catch (Exception e) {
            stackTrace = e.getStackTrace();
            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        he.close();
        
        if(stackTrace != null) // Send Event
            sendEvent(stackTrace);
        else if(good && evo != null) // Send email
            util.sendMail(evo);
    }
    
    @Override
    public boolean getRunning() {
        return running;
    }

    @Override
    public Logger getLogger() {
        return LOGGER;
    }
}
