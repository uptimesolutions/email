/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.http.handlers.multipart;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a file from an HTTP multipart/form-data request.
 * 
 * @author ksimmons
 */
public class FileItem {
    private String name; // The name of the field to which this file was associated in the HTTP request
    private String fileName; // The name of the file taken from the HTTP part (in the filename attribute of the Content-Disposition header)
    private String contentType; // The content type of the file taken from the Content-Type header of the part, null if not specified
    private long contentLength; // The content length of the file taken from the Content-Length header of the part, -1 if not specified
    private File file; // The file.
    private Map<String,String> headers; // The headers of the file part

    /**
     * Constructor.
     * @param fieldName the name of the field that holds the file.
     * @param fileName the name of the file.
     * @param contentType
     * @param contentLength
     * @param file
     * @param headers
     */
    public  FileItem(String fieldName, String fileName, String contentType, long contentLength, File file, Map<String,String> headers) {
        if (fieldName == null) 
            throw new IllegalArgumentException("no fieldName provided.");
        if (fileName == null) 
            throw new IllegalArgumentException("no fileName provided.");
        if (fileName == null) 
            throw new IllegalArgumentException("no inputStream provided");

        this.fileName = fileName;
        this.contentType = contentType;
        this.contentLength = contentLength;
        this.file = file;
        this.headers = headers;
        if (headers == null) {
            this.headers = new HashMap();
        }
    }

    public  String getName() {
            return name;
    }

    public  String getFileName() {
            return fileName;
    }

    public  String getContentType() {
            return contentType;
    }

    public  long getContentLength() {
            return contentLength;
    }

    public  File getFile() {
            return file;
    }

    public  Map<String, String> getHeaders() {
            return headers;
    }
}
