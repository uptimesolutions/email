/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.http.handlers.multipart.exceptions;

/**
 *
 * @author ksimmons
 */
public class MultipartException extends Exception {
    private static final long serialVersionUID = 1L;

    public  MultipartException() {
    }

    public  MultipartException(String message) {
        super(message);
    }

    public  MultipartException(Throwable cause) {
        super(cause);
    }

    public  MultipartException(String message, Throwable cause) {
        super(message, cause);
    }
}
