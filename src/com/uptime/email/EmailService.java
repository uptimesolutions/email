/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email;

import com.uptime.email.http.listeners.RequestListener;
import com.uptime.services.AbstractService;
import static com.uptime.services.AbstractService.getServiceHosts;
import static com.uptime.services.AbstractService.publishCircuitBreaker;
import static com.uptime.services.AbstractService.query;
import static com.uptime.services.AbstractService.register;
import static com.uptime.services.AbstractService.subscribe;
import static com.uptime.services.AbstractService.unregister;
import static com.uptime.services.AbstractService.unsubscribe;
import com.uptime.services.ServiceConstants;
import com.uptime.services.vo.ServiceHostVO;
import com.uptime.services.vo.EventVO;
import java.net.InetAddress;
import java.util.Date;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author ksimmons
 */
public class EmailService extends AbstractService {
    public final static String SERVICE_NAME = "Email";
    public static String IP_ADDRESS = null;
    public static int PORT = 0;
    public static final Logger LOGGER = Logger.getLogger(EmailService.class.getName());
    public static Semaphore mutex = new Semaphore(1);
    public static boolean running = true;
    private static RequestListener listener;
    public static String[] names = {"Events"};
    private static final AtomicLong EVENT_INDEX = new AtomicLong(0L);
    public static String MAIL_SERVER = null;
    
    //private final static boolean DEVELOPING_TESTING = true; // DEV
    //private final static boolean DEVELOPING_TESTING = false; // PROD
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // start the logger
            FileHandler fileTxt = new FileHandler("./servicelog.%u.%g.txt",1024*5120,5); 
            fileTxt.setFormatter(new SimpleFormatter());
            // remove the console logger from the root logger
            Logger rootLogger = LOGGER.getLogger("");
            Handler[] handlers = rootLogger.getHandlers();
            if (handlers[0] instanceof ConsoleHandler) {
                rootLogger.removeHandler(handlers[0]);
            }
            LOGGER.setUseParentHandlers(false);
            LOGGER.addHandler(fileTxt);

            System.out.println("PROP_FILE_LOCATION:"+System.getenv("PROP_FILE_LOCATION"));
            // get the network port number from the command line parameter
            if(args.length != 2){
                System.out.println("Usage: java EmailService.class port mail_server");
                System.exit(1);
            }
                
            // check port number id valid
            try{
                if((PORT = Integer.parseInt(args[0])) < 1024){
                    System.out.println("Port must be > 1024.");
                    System.exit(1);
                }
            } catch(NumberFormatException e) {
                System.out.println("Port must be an integer.");
                System.exit(1);
            }
            
            // check mail_server contains at least one . 
            if(!args[1].contains(".")){
                System.out.println("mail_server does not appear to be a valid FQDN or IP address.");
                System.exit(1);
            }
            MAIL_SERVER = args[1];
            
            // get IP address of local host
            IP_ADDRESS = InetAddress.getLocalHost().getHostAddress();
            
            startup();
            manageService();
            shutdown();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            System.exit(1);
        }
    }
    
    /**
     * Manage the service thread.
     */
    private static void manageService(){
        //Query Check
        while(running) {
            try {
                //Update required services
                if(running){
                    for(int i = 0; i < 300; i++){
                        if(!running) break;
                        Thread.sleep(1000);
                    }
                    
                    if (names != null) {
                        for(String name : names) {
                            try {
                                mutex.acquire();
                                if (!query(name, LOGGER)) {
                                    System.out.println("Query failed for " + name);
                                    LOGGER.log(Level.INFO, "Query failed for {0}", name);
                                }
                            } finally {
                                mutex.release();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                sendEvent(e.getStackTrace());
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            } 
        }
    }
    
    /**
     * Startup the service, start requestsListener, and registering the service
     * @throws Exception 
     */
    private static void startup() throws Exception{
        System.out.println("Startup initiated...");
        LOGGER.log(Level.INFO, "Startup initiated...");
        boolean success;
        
        // Starting Listener
        listener = new RequestListener(PORT);
        listener.start();
        
        try{
            mutex.acquire();
            
            // Registering
            System.out.println("Registering service instance...");
            LOGGER.log(Level.INFO, "Registering service instance...");
            do{
                if (!(success = register(SERVICE_NAME, IP_ADDRESS, PORT, LOGGER))) {
                    Thread.sleep(2000L);
                    System.out.println("Retry Registering service...");
                }
            }while(success == false);
            System.out.println("Service instance registered successfully.");
            LOGGER.log(Level.INFO, "Service instance registered successfully.");
            
            // Subscribing
            if (names != null) {
                System.out.println("Subscribing...");
                LOGGER.log(Level.INFO, "Subscribing...");
                do{
                    if (!(success = subscribe(names, IP_ADDRESS, PORT, "", "service", LOGGER))){
                        Thread.sleep(2000L);
                        System.out.println("Retry Subscribing...");
                    }
                }while(success == false);
                System.out.println("Subscribing successfully.");
                LOGGER.log(Level.INFO, "Subscribing successfully.");
            }
        } catch(Exception e){
            LOGGER.log(Level.SEVERE, "Startup failed.");
            LOGGER.log(Level.SEVERE, e.getMessage(),e);
        } finally {
            mutex.release();
        }
    }
    
    /**
     * Shuts down the service cleanly by unregistering the service, stopping all threads, and closing the Cassandra cluster connection.
     */
    private static void shutdown(){
        System.out.println("Shut down initiated...");
        LOGGER.log(Level.INFO, "Shut down initiated...");
        boolean success;
        
        try{
            // Unsubscribing
            if (names != null) {
                System.out.println("Unsubscribing...");
                LOGGER.log(Level.INFO, "Unsubscribing...");
                do{
                    if (!(success = unsubscribe(names, IP_ADDRESS, PORT, "", "service", LOGGER))){
                        Thread.sleep(2000L);
                        System.out.println("Retry Unsubscribing...");
                    }
                }while(success == false);
                System.out.println("Unsubscribing successfully.");
                LOGGER.log(Level.INFO, "Unsubscribing successfully.");
            }
            
            // Unregistering
            System.out.println("Unregistering service instance...");
            LOGGER.log(Level.INFO, "Unregistering service instance...");
            do{
                if (!(success = unregister(SERVICE_NAME, IP_ADDRESS, PORT, LOGGER))){
                    Thread.sleep(2000L);
                    System.out.println("Retry Unregistering service...");
                }
            }while(success == false);
            System.out.println("Service instance unregistered successfully.");
            LOGGER.log(Level.INFO, "Service instance unregistered successfully.");
        } catch(Exception e){
            sendEvent(e.getStackTrace());
            LOGGER.log(Level.SEVERE, e.getMessage(),e);
        }
        
        listener.stop();
        LOGGER.log(Level.INFO, "Exiting.");
        System.exit(0);
    }
    

    /**
     * Attempt to send an event to the Events service
     * @param stackTrace, Array of StackTraceElement objects
     */
    public static void sendEvent (StackTraceElement[] stackTrace){
        if(!ServiceConstants.DEVELOPING_TESTING) {
            StringBuilder data = new StringBuilder();
            EventVO evo;
            ServiceHostVO current; 
            String service = "Events";
            boolean emailing = true;
            int attempts, count = 0;
            Long createdDate = new Date().getTime();

            try {
                for(StackTraceElement ele : stackTrace){
                    data.append(ele.toString()).append("<br />");
                }

                // set EventVO
                evo = new EventVO();
                evo.setData(data.toString());
                evo.setApplication(SERVICE_NAME);
                evo.setIpAddress(IP_ADDRESS);
                evo.setPort(String.valueOf(PORT));
                evo.setCreatedDate(createdDate);

                while (emailing && count < 2) {

                    // Query Events service if needed
                    try {
                        mutex.acquire();
                        if(!getServiceHosts().containsKey(service) || getServiceHosts().get(service).isEmpty())
                            query(service, LOGGER);
                    }catch (Exception e) {
                        LOGGER.log(Level.SEVERE, e.getMessage(), e);
                    } finally {
                        mutex.release();
                    }

                    // Attempt to send event
                    if(!getServiceHosts().get(service).isEmpty()) {
                        attempts = 0;
                        do {
                            try {
                                current = getServiceHosts().get(service).get((int)EVENT_INDEX.get());
                                try {
                                    sendEvent(evo, current.getIp(), current.getPort()); // Sending Email
                                    attempts = 5;
                                    emailing = false;
                                } catch (Exception e) {
                                    LOGGER.log(Level.WARNING, e.getMessage(), e);

                                    // Publishing CircuitBreaker
                                    try{
                                        mutex.acquire();
                                        publishCircuitBreaker (current, service, LOGGER);
                                    }catch (Exception ex) {
                                        LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
                                    } finally {
                                        mutex.release();
                                    }
                                    ++attempts;
                                }
                                EVENT_INDEX.incrementAndGet();
                            } catch (IndexOutOfBoundsException e) {
                                if (EVENT_INDEX.get() == 0L)
                                    break;
                                else 
                                    EVENT_INDEX.set(0L);
                            } 
                        } while(attempts < 5);
                    }
                    ++count;
                }
            } catch (Exception e) {
                LOGGER.log(Level.WARNING, e.getMessage(), e);
            }
        }
    }

    @Override
    public void sendEmail(String json) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
