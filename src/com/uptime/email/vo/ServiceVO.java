/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.vo;

/**
 *
 * @author ksimmons
 */
public class ServiceVO {
    String service, ipaddress, port;
    boolean circuitBreaker;
    int index;

    /**
     * @return the service
     */
    public String getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(String service) {
        this.service = service;
    }

    /**
     * @return the ipaddress
     */
    public String getIpaddress() {
        return ipaddress;
    }

    /**
     * @param ipaddress the ipaddress to set
     */
    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the circuitBreaker
     */
    public boolean isCircuitBreaker() {
        return circuitBreaker;
    }

    /**
     * @param circuitBreaker the circuitBreaker to set
     */
    public void setCircuitBreaker(boolean circuitBreaker) {
        this.circuitBreaker = circuitBreaker;
    }

    /**
     * @return the index 
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set 
     */
    public void setIndex(int index) {
        this.index = index;
    }
}
