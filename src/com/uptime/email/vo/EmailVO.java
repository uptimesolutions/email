/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uptime.email.vo;

import java.io.File;
import java.util.List;

/**
 *
 * @author ksimmons
 */
public class EmailVO {
    private String fromEmail;
    private List<String> toEmails;
    private List<String> ccEmails;
    private String subject, mailBody;
    private List<String> attachments;
    private List<Integer> attachmentSizes;
    private List<File> attachmentContents;

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public List<String> getToEmails() {
        return toEmails;
    }

    public void setToEmails(List<String> toEmails) {
        this.toEmails = toEmails;
    }

    public List<String> getCcEmails() {
        return ccEmails;
    }

    public void setCcEmails(List<String> ccEmails) {
        this.ccEmails = ccEmails;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMailBody() {
        return mailBody;
    }

    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }

    public List<String> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<String> attachments) {
        this.attachments = attachments;
    }

    public List<Integer> getAttachmentSizes() {
        return attachmentSizes;
    }

    public void setAttachmentSizes(List<Integer> attachmentSizes) {
        this.attachmentSizes = attachmentSizes;
    }

    public List<File> getAttachmentContents() {
        return attachmentContents;
    }

    public void setAttachmentContents(List<File> attachmentContents) {
        this.attachmentContents = attachmentContents;
    }
}
